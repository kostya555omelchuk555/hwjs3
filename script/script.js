const number = parseInt(prompt("Enter a number:"));

if (isNaN(number)) {
  console.log("Invalid input. Please enter a number.");
} else {
  const multiplesOfFive = [];

  for (let i = 0; i <= number; i++) {
    if (i % 5 === 0) {
      multiplesOfFive.push(i);
    }
  }

  if (multiplesOfFive.length > 0) {
    console.log("Multiples of 5: ", multiplesOfFive);
  } else {
    console.log("Sorry, no numbers");
  }
}